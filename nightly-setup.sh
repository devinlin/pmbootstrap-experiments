#!/bin/bash

docker build ./container -t kde-nightly-builder

mkdir -p build
mkdir -p pmbootstrap-working-dir

# HACK: is there a better way to do this?? we need to give full permission to the container
chmod 777 build
chmod 777 pmbootstrap-working-dir
