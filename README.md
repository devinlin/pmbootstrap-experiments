# pmbootstrap-experiments

### Building container image

```bash
$ bash nightly-setup.sh
```

### Running builder

```bash
$ bash nightly-run.sh fetch
$ bash nightly-run.sh update
$ bash nightly-run.sh clear-package-cache
$ bash nightly-run.sh checksums
$ bash nightly-run.sh build
```

### Setting up a repository

The packages will be at `pmbootstrap-working-dir/packages`, which you can host somewhere.

You also need to add the public key at `pmbootstrap-working-dir/config_apk_keys/pmos@local...` to your system's `/etc/apk/keys` folder.

### Thanks

Derived from https://git.sr.ht/~z3ntu/plasma-mobile-nightly/tree/main