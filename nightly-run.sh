#!/bin/bash

command="$@"

docker run --rm -v ./src:/src \
                -v ./build:/home/builder/build \
                -v ./pmbootstrap-working-dir:/home/builder/.local/var/pmbootstrap \
                --cap-add SYS_ADMIN \
                --ulimit "nofile=1024:1048576" \  # Otherwise entering fakeroot is extremely slow
                -it kde-nightly-builder python /src/builder.py $command
