import os
import shutil
import subprocess
import argparse
import sys

import functions as funcs
import vars as vars

# Setup pmbootstrap
print('Setting up pmbootstrap...')
subprocess.run(['pmbootstrap', '-q', 'init'], input=b'\n')

# Ignore git warnings
subprocess.run(['git', 'config', '--global', '--add', 'safe.directory', "'*'"])

# Setup vars
apkbuild_overrides_dir = os.path.join(os.path.dirname(os.path.abspath(__file__)), 'apkbuild-overrides')
build_dir = os.path.join(os.getenv("HOME"), "build")
git_checkout_dir = os.path.join(build_dir, "git_checkouts")
tarball_dir = os.path.join(build_dir, "tarballs")
pmb_work_dir = subprocess.run(['pmbootstrap', 'config', 'work'], capture_output=True, text=True).stdout.strip()
pmaports_dir = subprocess.check_output(["pmbootstrap", "config", "aports"]).decode().strip().replace('$WORK', pmb_work_dir)

# Work in $HOME/build
os.chdir(build_dir)
os.makedirs(git_checkout_dir, exist_ok=True)
os.makedirs(tarball_dir, exist_ok=True)


def parse_cli():
    parser = argparse.ArgumentParser(
                        prog='plasma-pmos-builder')

    subparsers = parser.add_subparsers(title="Commands", dest="command")

    fetch_parser = subparsers.add_parser("fetch", help="Fetch package sources and recipes")
    fetch_parser.set_defaults(func=cli_fetch)

    update_parser = subparsers.add_parser("update", help="Patch package sources and create tarballs")
    update_parser.add_argument("--package", type=str, help="Update a single package")
    update_parser.set_defaults(func=cli_update)

    clear_package_cache_parser = subparsers.add_parser("clear-package-cache", help="Clear cached built packages")
    clear_package_cache_parser.set_defaults(func=cli_clear_package_cache)

    checksum_parser = subparsers.add_parser("checksum", help="Update package checksums")
    checksum_parser.add_argument("--package", type=str, help="Update checksum for a single package")
    checksum_parser.set_defaults(func=cli_checksum)

    build_parser = subparsers.add_parser("build", help="Build packages")
    build_parser.add_argument("--resume-from", type=str, help="Package to resume build from")
    build_parser.add_argument("--package", type=str, help="Build a single package")
    build_parser.set_defaults(func=cli_build)

    args = parser.parse_args()

    if args.command:
        args.func(args)
    else:
        parser.print_help()


def cli_fetch(args):
    subprocess.run(["pmbootstrap", "pull"])

    # Remove old kde-nightly APKBUILDs directory
    kde_nightly_dir = os.path.join(pmaports_dir, "kde-nightly")
    if os.path.exists(kde_nightly_dir) and os.path.isdir(kde_nightly_dir):
        shutil.rmtree(kde_nightly_dir)
    os.makedirs(kde_nightly_dir, exist_ok=True)

    # Call aportgen once to force clone aports_upstream
    print('Forcing the clone of aports_upstream...')
    subprocess.run(["pmbootstrap", "aportgen", "--fork-alpine", "extra-cmake-modules"])
    subprocess.run(["rm", "-rf", "temp/extra-cmake-modules/"])

    packages = funcs.get_package_list(pmb_work_dir)

    print("Packages to fetch:", packages)

    # Fork packages (APKBUILDs)
    src_dir = os.path.join(pmb_work_dir, 'cache_git', 'aports_upstream')
    for package in packages:
        src_package_dir = os.path.join(src_dir, package)
        dest_package_dir = os.path.join(pmaports_dir, 'kde-nightly', package)
        shutil.copytree(src_package_dir, dest_package_dir)

    # Update packages
    funcs.update_package_sources(
        packages=packages,
        git_checkout_dir=git_checkout_dir,
        pmaports_dir=pmaports_dir,
        num_workers=20)


def cli_update(args):
    if args.package is not None:
        # Update single package option
        funcs.update_package(
            pkgname=args.package,
            git_checkout_dir=git_checkout_dir,
            tarball_dir=tarball_dir,
            pmaports_dir=pmaports_dir,
            apkbuild_overrides_dir=apkbuild_overrides_dir
        )
    else:
        # Update and patch all packages
        packages = funcs.get_package_list(pmb_work_dir)
        funcs.update_packages(
            packages=packages,
            git_checkout_dir=git_checkout_dir,
            tarball_dir=tarball_dir,
            pmaports_dir=pmaports_dir,
            apkbuild_overrides_dir=apkbuild_overrides_dir,
            num_workers=1 # there appear to be race conditions
        )


def cli_clear_package_cache(args):
    # TODO: HACK remove extra-cmake-modules
    subprocess.run(["rm", "-rf", os.path.join(pmaports_dir, 'temp', 'extra-cmake-modules')])

    # Clear old packages
    subprocess.run(["pmbootstrap", "-y", "zap", "-hc", "-md"], input=b'\n')


def cli_checksum(args):
    # TODO: HACK remove extra-cmake-modules
    subprocess.run(["rm", "-rf", os.path.join(pmaports_dir, 'temp', 'extra-cmake-modules')])

    if args.package is not None:
        # Generate checksum for a single package
        result = subprocess.run(["pmbootstrap", "checksum", args.package])
        if result.returncode != 0:
            subprocess.run(["pmbootstrap", "log"])
            sys.exit(1)
    else:
        # Get full package list
        packages = funcs.get_package_list(pmb_work_dir)
        # Remove path prefixes
        packages = list(map(lambda p: funcs.remove_pkgname_prefix(p), packages))

        # Generate checksums for all packages
        for package in packages:
            result = subprocess.run(["pmbootstrap", "checksum", package])
            if result.returncode != 0:
                subprocess.run(["pmbootstrap", "log"])
                sys.exit(1)


def cli_build(args):
    # TODO: HACK remove extra-cmake-modules
    subprocess.run(["rm", "-rf", os.path.join(pmaports_dir, 'temp', 'extra-cmake-modules')])

    # HACK: for some reason sccache isn't pulled into the chroot
    # causes akonadi and angelfish to fail
    subprocess.run(["pmbootstrap", "chroot", "apk", "add", "sccache"])

    # Build a single package option (--package)
    if args.package is not None:
        print(f'Building [{args.package}]...')
        result = subprocess.run(["pmbootstrap", "build", args.package])
        if result.returncode != 0:
            subprocess.run(["pmbootstrap", "log"])
            sys.exit(1)
        return

    # Get package list
    packages = funcs.get_package_list(pmb_work_dir)
    # Remove path prefixes
    packages = list(map(lambda p: funcs.remove_pkgname_prefix(p), packages))

    print(f"Building packages... {packages}")

    needs_to_resume_from = args.resume_from is not None
    if needs_to_resume_from:
        print(f"Resuming build from {args.resume_from}")
    found_resume_from_pkg = False

    # Build all packages
    for package in packages:

        # Loop until we get to the package to resume from
        if needs_to_resume_from and not found_resume_from_pkg:
            if package == args.resume_from:
                found_resume_from_pkg = True
            else:
                continue

        print(f'[{package}] Building [{package}]...')
        result = subprocess.run(["pmbootstrap", "build", package])
        if result.returncode != 0:
            subprocess.run(["pmbootstrap", "log"])
            sys.exit(1)

    print("Done!")


parse_cli()