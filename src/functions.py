from concurrent.futures import ThreadPoolExecutor, as_completed
import fnmatch
import os
import re
import shutil
import subprocess
from datetime import datetime

import vars as vars


def delete_package(pkgname, packages):
    packages = [pkg for pkg in packages if pkgname not in pkg]
    return packages


def get_package_list(pmb_work_dir):
    # Get git packages from pmOS package groups
    packages = []
    for group in ["kde-frameworks", "kde-plasma", "kde-mobile", "kde-applications", "kde-other"]:
        packages.extend(get_git_packages(group, pmb_work_dir))

    # Filter out certain packages
    for exclude in vars.excluded_packages:
        packages = delete_package(exclude, packages)

    return packages


def pkgname_to_gitlab_name(pkgname):
    gitlab_name = remove_pkgname_prefix(pkgname)

    # Map pmOS package name -> gitlab repo name
    if gitlab_name in vars.package_name_to_repo_name:
        gitlab_name = vars.package_name_to_repo_name[gitlab_name]

    return gitlab_name


def get_apkbuild_folder(pkgname, pmaports_dir):
    return os.path.join(pmaports_dir, f"kde-nightly/{pkgname}")


def get_apkbuild_path(pkgname, pmaports_dir):
    return os.path.join(get_apkbuild_folder(pkgname, pmaports_dir), "APKBUILD")


def get_apkbuild_contents(pkgname, pmaports_dir):
    apkbuild = get_apkbuild_path(pkgname, pmaports_dir)
    with open(apkbuild, 'r') as file:
        content = file.read()
    return content


def get_git_packages(group, pmb_work_dir):
    # Run the git command and return the list of packages
    path = os.path.join(pmb_work_dir, 'cache_git', 'aports_upstream')
    result = subprocess.run(
        ['git', '-C', path, 'grep', '-l', f'group={group}'],
        capture_output=True, text=True
    )
    # Split the output and use only the first and second components of the paths
    return ["/".join(line.split('/')[:2]) for line in result.stdout.splitlines()]


def remove_pkgname_prefix(pkgname):
    return pkgname.replace('community/', '').replace('testing/', '')


def update_package_source(pkgname, git_checkout_dir, pmaports_dir):
    print(f'[{pkgname}] Updating source...')
    apkbuild_contents = get_apkbuild_contents(pkgname, pmaports_dir)

    # Check for _repo_url in APKBUILD
    if '_repo_url=' not in apkbuild_contents:
        print(f"[{pkgname}] APKBUILD is missing _repo_url!")
        return False
    repo_url = apkbuild_contents.split('_repo_url=')[1].split('"')[1]
    gitlab_name = pkgname_to_gitlab_name(pkgname)

    # Clone from GitLab, if it doesn't already exist
    clone_path = os.path.join(git_checkout_dir, gitlab_name)
    if not os.path.isdir(clone_path):
        print(f"[{gitlab_name}] has no local checkout yet, cloning from {repo_url}")
        subprocess.run(["git", "clone", repo_url, clone_path])

    wd = os.getcwd()
    os.chdir(clone_path)

    # Update the repo
    subprocess.run(["git", "pull"], stdout=subprocess.DEVNULL, stderr=subprocess.STDOUT)

    os.chdir(wd)
    return clone_path


def update_package(pkgname, git_checkout_dir, tarball_dir, pmaports_dir, apkbuild_overrides_dir):
    print(f"[{pkgname}] Updating...")
    gitlab_name = pkgname_to_gitlab_name(pkgname)

    clone_path = os.path.join(git_checkout_dir, gitlab_name)
    os.chdir(clone_path)

    # Current commit information
    commit = subprocess.check_output(["git", "rev-parse", "HEAD"]).decode().strip()
    commit_info = subprocess.check_output(['git', 'show', '-s', '--format=%cI', commit], text=True, stderr=subprocess.STDOUT).strip()

    # Split the output to extract year, month, and day
    commit_date = commit_info.split('T')[0]  # Get the date part (YYYY-MM-DD)
    commit_year, commit_month, commit_day = commit_date.split('-')

    # Create source code tarball
    tarball_name = f"{gitlab_name}-{commit}.tar.xz"
    if not os.path.exists(os.path.join(tarball_dir, tarball_name)):
        print(f"[{pkgname}] Source has been updated, creating new tarball...")

        # Delete old tars (any with gitlab_name as prefix)
        for file in os.listdir(tarball_dir):
            # Get the string up to the last occurrence of the '-'
            if gitlab_name == file.rpartition('-')[0]:
                os.remove(os.path.join(tarball_dir, file))

        # Create tarball
        subprocess.run(["git", "archive", "--format", "tar", "--prefix", f"{gitlab_name}-{commit}/", "HEAD"], stdout=open(f"{tarball_dir}/{gitlab_name}-{commit}.tar", 'wb'))
        subprocess.run(["xz", "-vv", "-T0", "-9", "-e", f"{tarball_dir}/{gitlab_name}-{commit}.tar"])

    # Update APKBUILD
    apkbuild = get_apkbuild_path(pkgname, pmaports_dir)

    # Check if there is an APKBUILD override, and override
    apkbuild_override_path = os.path.join(apkbuild_overrides_dir, f"{remove_pkgname_prefix(pkgname)}-APKBUILD")
    if os.path.exists(apkbuild_override_path):
        shutil.copyfile(apkbuild_override_path, apkbuild)

    # Update APKBUILD details to build from dev
    with open(apkbuild, 'r') as file:
        apkbuild_content = file.read()

    today = datetime.now().date().isoformat()
    apkbuild_content = re.sub(r'^pkgrel=.*', f'pkgrel=0', apkbuild_content, flags=re.MULTILINE)
    apkbuild_content = re.sub(r'^pkgver=.*', f'pkgver=9999_git{commit_year}{commit_month}{commit_day}', apkbuild_content, flags=re.MULTILINE)

    # HACK: added setcap option for kwin, but we need a better way to append to options, not just replace it
    apkbuild_content = re.sub(r'build\(\)', 'options="!check setcap"\nbuild()', apkbuild_content)
    apkbuild_content = re.sub(r'build\(\)', f'_commit="{commit}"\nbuild()', apkbuild_content)
    apkbuild_content = re.sub(
        r'build\(\)',
        f'builddir="$srcdir/{gitlab_name}-$_commit"\n\nbuild()',
        apkbuild_content
    )

    # Update tarball source line.
    # We need to account for if there's only 1 or more sources (tarball is expected to be first),
    # adjusting for ending " if necessary.
    if re.search(r'^source=.*"$', apkbuild_content, re.MULTILINE):
        apkbuild_content = re.sub(r'source=.*"$', f'source="{tarball_name}"', apkbuild_content, flags=re.MULTILINE)
    else:
        apkbuild_content = re.sub(r'source=.*[^\"]$', f'source="{tarball_name}', apkbuild_content, flags=re.MULTILINE)

    # Copy tarball into APKBUILD folder as source
    shutil.copy(os.path.join(tarball_dir, tarball_name), get_apkbuild_folder(pkgname, pmaports_dir))

    with open(apkbuild, 'w') as file:
        file.write(apkbuild_content)

    return True


def update_package_sources(num_workers, packages, git_checkout_dir, pmaports_dir):
    with ThreadPoolExecutor(max_workers=num_workers) as executor:
        futures = {
            executor.submit(update_package_source, pkgname, git_checkout_dir, pmaports_dir): pkgname
            for pkgname in packages
        }

        # As each task completes, get the result
        for future in as_completed(futures):
            pkgname = futures[future]
            try:
                result = future.result()
                print(f"[{pkgname}] Completed source update: {result}")
            except Exception as exc:
                print(f"[{pkgname}] Source update generated an exception: {exc}")


def update_packages(num_workers, packages, git_checkout_dir, tarball_dir, pmaports_dir, apkbuild_overrides_dir):
    with ThreadPoolExecutor(max_workers=num_workers) as executor:
        futures = {
            executor.submit(update_package, pkgname, git_checkout_dir, tarball_dir, pmaports_dir, apkbuild_overrides_dir): pkgname
            for pkgname in packages
        }

        # As each task completes, get the result
        for future in as_completed(futures):
            pkgname = futures[future]
            try:
                result = future.result()
                print(f"[{pkgname}] Completed update: {result}")
            except Exception as exc:
                print(f"[{pkgname}] Update generated an exception: {exc}")
