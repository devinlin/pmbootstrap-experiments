#!/bin/bash
# shellcheck disable=SC2016

set -e

cd $HOME/build

# Setup pmbootstrap
yes "" | pmbootstrap -q init

. /scripts/functions.sh

# General idea:
# Patch APKBUILD to pull tarball (from a local cgit or something that mirrors everything, to reduce load on invent.kde.org)
# Let pmbootstrap figure out the dependency order and build everything

git_checkout_dir="$HOME/build/git_checkouts"
tarball_dir="$HOME/build/tarballs"
pmaports_dir="$(pmbootstrap config aports)"

mkdir -p "$git_checkout_dir"
mkdir -p "$tarball_dir"

rm -rf "$pmaports_dir"/kde-nightly/
mkdir -p "$pmaports_dir"/kde-nightly/

cd "$pmaports_dir"

echo "Setting up..."
pmbootstrap pull
pmbootstrap aportgen --fork-alpine extra-cmake-modules # call it once to force clone aports_upstream
rm -rf temp/extra-cmake-modules/

packages=($(git -C "$(pmbootstrap config work)"/cache_git/aports_upstream grep -l group=kde-frameworks | cut -d'/' -f1,2))
packages+=($(git -C $(pmbootstrap config work)/cache_git/aports_upstream grep -l group=kde-plasma | cut -d'/' -f1,2))
packages+=($(git -C $(pmbootstrap config work)/cache_git/aports_upstream grep -l group=kde-mobile | cut -d'/' -f1,2))
# Maui is still broken
#packages+=($(git -C $(pmbootstrap config work)/cache_git/aports_upstream grep -l group=maui | cut -d'/' -f1,2))
packages+=($(git -C $(pmbootstrap config work)/cache_git/aports_upstream grep -l group=kde-applications | cut -d'/' -f1,2))
packages+=($(git -C $(pmbootstrap config work)/cache_git/aports_upstream grep -l group=kde-other | cut -d'/' -f1,2))

echo "Deleting removed packages..."

# remove other removed packages
delete_package phonon-backend-gstreamer

# Don't bother with these yet
delete_package aura-browser
delete_package plank-player
delete_package plasma-remotecontrollers
delete_package plasma-bigscreen
delete_package digikam
delete_package kdenlive
delete_package kgraphviewer

# kaccount doesn't build yet due to libaccounts-qt
delete_package kaccounts-integration
delete_package kaccounts-providers

# TODO: handle meta packages later
delete_package plasma-desktop-meta
delete_package plasma-bigscreen-meta
delete_package plasma-mobile-meta
delete_package kde-applications

# not (properly) ported to Qt6/KF6 yet
delete_package alkimia
delete_package artikulate
delete_package audiotube
delete_package cantor
delete_package cervisia
delete_package choqok
delete_package falkon
delete_package juk
delete_package kajongg
delete_package kamoso
delete_package kde-dev-utils
delete_package kdesdk-thumbnailers
delete_package kdevelop
delete_package kget
delete_package kgpg
delete_package kig
delete_package kile
delete_package kio-fuse
delete_package kio-gdrive
delete_package kipi-plugins
delete_package kiten
delete_package kmix
delete_package kmouth
delete_package kmymoney
delete_package knights
delete_package konqueror
delete_package konversation
delete_package kopete
delete_package kphotoalbum
delete_package krecorder
delete_package krename
delete_package krfb
delete_package ktorrent
delete_package ktouch
delete_package kwave
delete_package libkipi
delete_package lokalize
delete_package marble
delete_package massif-visualizer
delete_package okteta
delete_package okular
delete_package parley
delete_package peruse
delete_package poxml
delete_package rkward
delete_package rocs
delete_package skrooge
delete_package subtitlecomposer
delete_package umbrello

# no need
delete_package qca

# Delete all packages suffixed with 5 (Plasma5/KF5)
for package in "${packages[@]}"; do
    if [[ $package == *5 ]]; then
        delete_package $package
    fi
done

# fork all packages
echo "Copying all packages from aports..."
for package in "${packages[@]}"; do
    cp -r $(pmbootstrap config work)/cache_git/aports_upstream/"$package" kde-nightly/
done

packages=("${packages[@]/community\//}")
packages=("${packages[@]/testing\//}")

# combine forked packages and new packages into a single array
packages=("${packages[@]}" "${new_packages[@]}")

for pkgname in "${packages[@]}"; do
    apkbuild="kde-nightly/$pkgname/APKBUILD"
    echo "Preparing $pkgname"

    # TODO: still necessary?
    sed -i 's/phonon-backend-gstreamer/phonon-backend-vlc/g' "$apkbuild"

    if [ "$pkgname" == ktexttemplate ]; then
        sed -i 's/$pkgname-lang//' "$apkbuild"
    elif [ "$pkgname" == solid ]; then
        # Symlinks are ignored by default_libs but we need it in -libs
        # So we do it manually
        cat <<EOF >> "$apkbuild"
libs() {
	default_libs

	amove usr/lib/libKF6Solid.so.6
}
EOF
    fi

    # Remove any patches, they most of the time do not apply and should need proper fixing upstream anyway
    # sed -i 's/000.*patch//' "$apkbuild"

    # fixup gitlab repo names vs pkgname
    gitlab_name=$pkgname
    if [ "$pkgname" == kirigami2 ]; then
        gitlab_name=kirigami
    elif [ "$pkgname" == oxygen-icons ]; then
        gitlab_name=oxygen-icons5
    elif [ "$pkgname" == kdeconnect ]; then
        gitlab_name=kdeconnect-kde
    elif [ "$pkgname" == index ]; then
	    gitlab_name=index-fm
    elif [ "$pkgname" == phonon-backend-vlc ]; then
	    gitlab_name=phonon-vlc
    elif [ "$pkgname" == polkit-qt ]; then
        gitlab_name=polkit-qt-1
    fi

    sed -i "s|build()|options=\"!check\"\nbuild()|" "$apkbuild"
    sed -i "s|build()|builddir=\"\$srcdir/$gitlab_name-\$_commit\"\n\nbuild()|" "$apkbuild"

    update_package "$apkbuild" "$gitlab_name"

    if [ "$pkgname" == mauiman ]; then
        # The APKBUILD already has this set but is filled with the wrong pkgver set at the top
        sed -i 's/$pkgname=$pkgver-r$pkgrel//' "$apkbuild"
        echo 'depends_dev="$depends_dev $pkgname=$pkgver-r$pkgrel"' >> kde-nightly/mauiman/APKBUILD
    fi
done

echo "Updating checksums..."
for pkgname in "${packages[@]}"; do
    pmbootstrap checksum "$pkgname" # > /dev/null 2>&1
done

echo "pkgver=9999_git00000000" >> main/postmarketos-ui-plasma-mobile/APKBUILD

set -x

# Clear out outdated packages and HTTP cache
pmbootstrap -y zap -hc -md

pmbootstrap build --strict "${packages[@]}"
set +x

echo "Done!"
