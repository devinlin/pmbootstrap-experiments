#!/bin/bash
# shellcheck disable=SC2016

set -e

cd /build

. /scripts/functions.sh

pmaports_dir="$(pmbootstrap config aports)"

cd "$pmaports_dir"

echo "Setting up..."
pmbootstrap pull > /dev/null 2>&1

packages=($(git -C "$(pmbootstrap config aports)" grep -l group=kde-frameworks | cut -d '/' -f 1,2))
#packages+=($(git -C "$(pmbootstrap config aports)" grep -l group=maui | cut -d '/' -f 1,2))
packages+=($(git -C "$(pmbootstrap config aports)" grep -l group=kde-plasma | cut -d '/' -f 1,2))
packages+=($(git -C "$(pmbootstrap config aports)" grep -l group=kde-mobile | cut -d '/' -f 1,2))
packages+=($(git -C "$(pmbootstrap config aports)" grep -l group=kde-applications | cut -d '/' -f 1,2))
packages+=($(git -C "$(pmbootstrap config aports)" grep -l group=kde-other | cut -d '/' -f 1,2))
packages+=($(git -C "$(pmbootstrap config aports)" grep -l group=3rdparty | cut -d '/' -f 1,2))

packages=("${packages[@]/kde6-rc\//}")

delete_package kdenlive

set -x

# Clear out outdated packages and HTTP cache
pmbootstrap -y zap -hc -md

pmbootstrap build --strict "${packages[@]}"
set +x

echo "Clearing old packages from repo"
ssh proxmox1.postmarketos.org -- rm -r /var/www/nightly/plasma-mobile/packages-beta/ > /dev/null 2>&1 || true
echo "Uploading packages to nightly.postmarketos.org..."
rsync --archive --delete -v "$(pmbootstrap config work)"/packages/edge/* proxmox1.postmarketos.org:/var/www/nightly/plasma-mobile/packages-beta/master/ > /dev/null 2>&1

echo "Done!"
