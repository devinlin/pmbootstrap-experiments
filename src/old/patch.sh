#!/bin/bash
# shellcheck disable=SC2016

set -e

cd /build

. /scripts/functions.sh

# General idea:
# Patch APKBUILD to pull tarball (from a local cgit or something that mirrors everything, to reduce load on invent.kde.org)
# Let pmbootstrap figure out the dependency order and build everything

git_checkout_dir="$HOME/build/git_checkouts"
tarball_dir="$HOME/build/tarballs"
pmaports_dir="$(pmbootstrap config aports)"

cd "$pmaports_dir"

echo "Setting up..."
pmbootstrap pull > /dev/null 2>&1

packages=($(git -C "$(pmbootstrap config aports)" grep -l group=kde-frameworks | cut -d '/' -f 1,2))
#packages+=($(git -C "$(pmbootstrap config aports)" grep -l group=maui | cut -d '/' -f 1,2))
packages+=($(git -C "$(pmbootstrap config aports)" grep -l group=kde-plasma | cut -d '/' -f 1,2))
packages+=($(git -C "$(pmbootstrap config aports)" grep -l group=kde-mobile | cut -d '/' -f 1,2))
packages+=($(git -C "$(pmbootstrap config aports)" grep -l group=kde-applications | cut -d '/' -f 1,2))
packages+=($(git -C "$(pmbootstrap config aports)" grep -l group=kde-other | cut -d '/' -f 1,2))
packages+=($(git -C "$(pmbootstrap config aports)" grep -l group=3rdparty | cut -d '/' -f 1,2))

packages=("${packages[@]/kde-nightly\//}")

# Requires currently unmerged PRs
delete_package oxygen

echo "\n"
for pkgname in "${packages[@]}"; do
    apkbuild="kde-nightly/$pkgname/APKBUILD"
    echo -e "\e[1A\e[KPreparing $pkgname"

    # fixup gitlab repo names vs pkgname
    gitlab_name=$pkgname
    if [ "$pkgname" == kirigami2 ]; then
        gitlab_name=kirigami
    elif [ "$pkgname" == oxygen-icons ]; then
        gitlab_name=oxygen-icons5
    elif [ "$pkgname" == kdeconnect ]; then
        gitlab_name=kdeconnect-kde
    elif [ "$pkgname" == index ]; then
        gitlab_name=index-fm
    elif [ "$pkgname" == phonon-backend-vlc ]; then
        gitlab_name=phonon-vlc
    elif [ "$pkgname" == polkit-qt ]; then
        gitlab_name=polkit-qt-1
    fi

    update_package "$apkbuild" "$gitlab_name"
done

echo "Uploading source files to nightly.postmarketos.org..."
rsync --archive --delete -v "$tarball_dir"/* proxmox1.postmarketos.org:/var/www/nightly/plasma-mobile/sources/$(date -I) > /dev/null  2>&1

echo "Updating checksums..."
for pkgname in "${packages[@]}"; do
    pmbootstrap checksum "$pkgname" > /dev/null 2>&1
done

set -x

# Clear out outdated packages and HTTP cache
pmbootstrap -y zap -hc -md

packages+=("postmarketos-ui-plasma-mobile")

pmbootstrap build --strict "${packages[@]}"
set +x

# echo "Clearing old packages from repo"
# ssh proxmox1.postmarketos.org -- rm -r /var/www/nightly/plasma-mobile/packages/master/ > /dev/null 2>&1
# echo "Uploading packages to nightly.postmarketos.org..."
# rsync --archive --delete -v "$(pmbootstrap config work)"/packages/edge/* proxmox1.postmarketos.org:/var/www/nightly/plasma-mobile/packages/master/ > /dev/null 2>&1

echo "Done!"
