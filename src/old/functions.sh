delete_package() {
    target="$1"
    for i in "${!packages[@]}"; do
        if [[ ${packages[i]} == *"$target" ]]; then
            unset 'packages[i]'
        fi
    done
}

delete_dependency_from_apkbuild() {
    apkbuild=$1
    dependency=$2

    # Change where dependency is on it's own line
    sed -i "s/\t$dependency-dev//" "$apkbuild"

    # Change where dependency is not on it's own line
    sed -i "s/$dependency-dev//" "$apkbuild"

    sed -i "s/\t$dependency//" "$apkbuild"
    sed -i "s/$dependency//" "$apkbuild"
}

update_package() {
    apkbuild=$1
    gitlab_name=$2

    if ! grep -q _repo_url= "$apkbuild" > /dev/null 2>&1; then
        echo "$apkbuild is missing _repo_url!"
        exit 1
    fi
    repo_url=$(grep _repo_url= "$apkbuild" | cut -d '"' -f 2)

    pushd $git_checkout_dir > /dev/null
    if [ ! -d "$gitlab_name" ]; then
        echo -e "\e[1A\e[K$gitlab_name has no local checkout yet, cloning from $repo_url"

        git clone "$repo_url" # > /dev/null 2>&1
    fi

    cd "$gitlab_name"

    package_updated_today=false
    # Let's make sure we only update once a day so we can run the script multiple times if we want too
    # without builds changing unintentionally in between
    if grep -Fq "$gitlab_name=" "$HOME/package-update-$(date -I)" 2>&1; then
        package_updated_today=true
    fi

    if ! $package_updated_today; then
        # Some packages do the Qt6/KF6 porting in a separate kf6 branch
        if [ $gitlab_name != kclock ] && [ $gitlab_name != kweather ] && [ $gitlab_name != print-manager ] && [ $gitlab_name != mimetreeparser ] && [ $gitlab_name != plasma-phonebook ]; then
            git checkout kf6 > /dev/null 2>&1 || true
        fi

        if [ $gitlab_name == khelpcenter ] || [ $gitlab_name == dolphin ]; then
            # These keep a kf6 branch around that shouldn't actually be used
            git checkout master > /dev/null 2>&1 || true
        fi
    fi

    previous_commit="$(git rev-parse HEAD)"

    if ! $package_updated_today; then
        git pull > /dev/null 2>&1
    fi

    commit="$(git rev-parse HEAD)"
    commit_year="$(git show -s --format='%cI' $commit | cut -d '-' -f 1)"
    commit_month="$(git show -s --format='%cI' $commit | cut -d '-' -f 2)"
    commit_day="$(git show -s --format='%cI' $commit | cut -d 'T' -f 1 | cut -d '-' -f 3)"

    if [ ! -f $tarball_dir/$gitlab_name-$commit.tar.xz ]; then
        echo -e "\e[1A\e[K$gitlab_name updated, creating new tarball..."
        # Make sure to clear the old tarball
        if [ -f $tarball_dir/$gitlab_name-$previous_commit.tar.xz ] && [ "$previous_commit" != "$_commit" ]; then
            rm $tarball_dir/$gitlab_name-$previous_commit.tar.xz
        fi

        git archive --format tar --prefix=$gitlab_name-$commit/ HEAD > $tarball_dir/$gitlab_name-$commit.tar
        xz -vv -T0 -9 -e "$tarball_dir/$gitlab_name-$commit.tar" # > /dev/null 2>&1
    fi

    popd > /dev/null

    sed -i "s|^pkgrel=.*|pkgrel=0|" "$apkbuild"
    sed -i "s|^_commit=.*|_commit=\"$commit\"|" "$apkbuild"
    if grep 'source=' "$apkbuild" | grep '"$' > /dev/null 2>&1; then
        sed -i "s|source=.*|source=\"https://nightly.postmarketos.org/plasma-mobile/sources/$(date -I)/$gitlab_name-\$_commit.tar.xz\"|" "$apkbuild"
    else
        sed -i "s|source=.*|source=\"https://nightly.postmarketos.org/plasma-mobile/sources/$(date -I)/$gitlab_name-\$_commit.tar.xz|" "$apkbuild"
    fi
    sed -i "s|^pkgver=.*|pkgver=9999_git$commit_year$commit_month$commit_day|" "$apkbuild"

    if ! $package_updated_today; then
        echo "$gitlab_name=$commit" >> "$HOME/package-update-$(date -I)"
    fi
}